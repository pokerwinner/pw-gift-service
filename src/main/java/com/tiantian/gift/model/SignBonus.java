package com.tiantian.gift.model;

import java.util.List;

/**
 * 签到额外奖励
 */
public class SignBonus {
    private String code;
    private List<Gift> giftList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Gift> getGiftList() {
        return giftList;
    }

    public void setGiftList(List<Gift> giftList) {
        this.giftList = giftList;
    }
}
