package com.tiantian.gift.server;

import com.tiantian.gift.data.postgresql.PGDatabase;
import com.tiantian.gift.handler.GiftHandler;
import com.tiantian.gift.settings.GiftConfig;
import com.tiantian.gift.settings.PGConfig;
import com.tiantian.gift.thrift.GiftService;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class GiftServer {
    private Logger LOG= LoggerFactory.getLogger(GiftServer.class);
    private TThreadPoolServer server;
    private TThreadPoolServer.Args sArgs;
    public static void main(String [] args) {
        GiftServer accountServer = new GiftServer();
        accountServer.init(args);
        accountServer.start();
    }
    public void init(String[] arguments) {
        LOG.info("init");
        try {
            PGDatabase.getInstance().init(
                    PGConfig.getInstance().getHost(),
                    PGConfig.getInstance().getPort(),
                    PGConfig.getInstance().getDb(),
                    PGConfig.getInstance().getUsername(),
                    PGConfig.getInstance().getPassword()
            );
            GiftHandler handler = new GiftHandler();
            TProcessor tprocessor = new GiftService.Processor<GiftService.Iface>(handler);

            TServerSocket tss = new TServerSocket(GiftConfig.getInstance().getPort());

            sArgs = new TThreadPoolServer.Args(tss);

            sArgs.processor(tprocessor);

            sArgs.transportFactory(new TFastFramedTransport.Factory());

            sArgs.protocolFactory(new TCompactProtocol.Factory());
        } catch (Exception x) {
            x.printStackTrace();
        }

    }

    public void start() {
        LOG.info("start");
        server = new TThreadPoolServer(sArgs);
        LOG.info("the server listen in {}", GiftConfig.getInstance().getPort());
        server.serve();
    }

    public void stop() {
        LOG.info("stop");
        server.stop();
    }

    public void destroy() {
        LOG.info("destroy");
    }
}
