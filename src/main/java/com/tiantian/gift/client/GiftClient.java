package com.tiantian.gift.client;

import com.tiantian.gift.settings.GiftConfig;
import com.tiantian.gift.thrift.GiftService;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

/**
 *
 */
public class GiftClient {
    private TTransport transport;
    private GiftService.Client client;

    public GiftService.Client getClient() {
        return client;
    }

    public void setClient(GiftService.Client client) {
        this.client = client;
    }

    public GiftClient() {
        try {
            transport = new TSocket(GiftConfig.getInstance().getHost(),
                    GiftConfig.getInstance().getPort());
            transport.open();
            TFastFramedTransport fastTransport = new TFastFramedTransport(transport);

            TCompactProtocol protocol = new TCompactProtocol(fastTransport);

            client = new GiftService.Client(protocol);
        } catch (TException x) {
            x.printStackTrace();
        }
    }

    public void close() {
        if (null != transport) {
            transport.close();
            client = null;
        }
    }
}
