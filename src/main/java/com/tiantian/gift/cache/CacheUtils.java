package com.tiantian.gift.cache;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.tiantian.gift.cache.model.Goods;
import com.tiantian.gift.cache.model.Props;
import com.tiantian.gift.data.redis.RedisUtil;
import com.tiantian.gift.model.Gift;

import java.util.*;

/**
 *
 */
public class CacheUtils {
    private static final String PROP_KEY = "prop:";
    private static final String GOODS_KEY = "goods:";
    private static final String SIGN_GIFT_KEY = "sign_gift:";
    private static final String SIGN_BONUS_GIFT_KEY = "sign_bonus_gift:";
    public static List<Props> getAllProps(){
        String value = RedisUtil.get(PROP_KEY);
        if (value == null) {
            return new ArrayList<>();
        }
        List propsList = JSON.parseArray(value, Props.class);
        if (propsList == null) {
            propsList = new ArrayList<>();
        }
        return propsList;
    }

    public static List<Goods> getAllGoods(){
        String value = RedisUtil.get(GOODS_KEY);
        if (value == null) {
            return new ArrayList<>();
        }
        List goodsList = JSON.parseArray(value, Goods.class);
        if (goodsList == null) {
            goodsList = new ArrayList<>();
        }
        return goodsList;
    }

    public static Map<String, Gift> getSignGifts(){
        String value = RedisUtil.get(SIGN_GIFT_KEY);
        if (value == null) {
            return new HashMap<>();
        }
        Map<String, Gift> giftMap = new HashMap<>();
        JSONObject giftsJson = JSON.parseObject(value);
        Set<String> keys = giftsJson.keySet();
        for (String key : keys) {
             JSONObject jsonObject =  giftsJson.getJSONObject(key);
             Gift gift = JSON.toJavaObject(jsonObject, Gift.class);
             giftMap.put(key, gift);
        }
        return giftMap;
    }

    public static  Map<String, Gift> getSignBonusGifts(){
        String value = RedisUtil.get(SIGN_BONUS_GIFT_KEY);
        if (value == null) {
            return new HashMap<>();
        }
        Map<String, Gift> giftMap = new HashMap<>();
        JSONObject giftsJson = JSON.parseObject(value);
        Set<String> keys = giftsJson.keySet();
        List<String> list = new ArrayList<>(keys);
        Collections.sort(list);
        int index = 1;
        for (String key : list) {
             JSONObject jsonObject = giftsJson.getJSONObject(key);
             Gift gift = JSON.toJavaObject(jsonObject, Gift.class);
             giftMap.put(index + "", gift);
             index ++;
        }
        return giftMap;
    }
}
