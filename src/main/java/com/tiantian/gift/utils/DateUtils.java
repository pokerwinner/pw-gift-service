package com.tiantian.gift.utils;

import java.util.Calendar;
import java.util.Date;

/**
 *
 */
public class DateUtils {
    public static boolean checkChinaSameWeek(long time1, long time2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(new Date(time1));
        cal2.setTime(new Date(time2));
        int dayWeek1 = cal1.get(Calendar.DAY_OF_WEEK);
        int dayWeek2 = cal2.get(Calendar.DAY_OF_WEEK);
        if(dayWeek1 == 1) {
            time1 -= 7 * 24 * 3600000;
            cal1.setTime(new Date(time1));
        }
        int week1 = cal1.get(Calendar.WEEK_OF_YEAR);

        if (dayWeek2 == 1) {
            time2 -= 7 * 24 * 3600000;
            cal2.setTime(new Date(time2));
        }
        int week2 = cal2.get(Calendar.WEEK_OF_YEAR);
        return week1 == week2;
    }
}
