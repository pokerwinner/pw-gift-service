package com.tiantian.gift.utils;

import com.tiantian.gift.client.GiftClient;
import com.tiantian.gift.client.GiftFactory;
import com.tiantian.gift.settings.GiftConfig;
import com.tiantian.gift.thrift.CommonSignInfo;
import com.tiantian.gift.thrift.GiftInfo;
import com.tiantian.gift.thrift.UserSignInfo;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import java.util.Map;

/**
 *
 */
public class GiftUtils {
    private ObjectPool<GiftClient> pool;
    private GiftUtils(){
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setTestOnBorrow(GiftConfig.getInstance().isTestOnBorrow());
        config.setTestOnCreate(GiftConfig.getInstance().isTestOnCreate());
        pool = new GenericObjectPool<GiftClient>(new GiftFactory(), config);
    }
    private static class GiftUtilsHolder{
        private final static GiftUtils instance = new GiftUtils();
    }
    public static GiftUtils getInstance(){
        return GiftUtilsHolder.instance;
    }

    public String getServiceVersion(){
        GiftClient client = null;
        String version = "";
        try {
            client = pool.borrowObject();
            version=client.getClient().getServiceVersion();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        }finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return version;
    }

    public UserSignInfo userSignInfo(String userId){
        GiftClient client = null;
        UserSignInfo result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().userSignInfo(userId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        }finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public Map<String, GiftInfo> userSign(String userId){
        GiftClient client = null;
        Map<String, GiftInfo> result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().userSign(userId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        }finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public CommonSignInfo getCommonGiftInfo() {
        GiftClient client = null;
        CommonSignInfo result = null;
        try {
            client = pool.borrowObject();
            result = client.getClient().getCommonGiftInfo();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        }finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public String giftMd5() {
        GiftClient client = null;
        String result = "";
        try {
            client = pool.borrowObject();
            result = client.getClient().giftMd5();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        }finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
}
