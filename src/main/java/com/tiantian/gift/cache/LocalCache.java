package com.tiantian.gift.cache;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.tiantian.gift.cache.model.Goods;
import com.tiantian.gift.cache.model.Props;
import com.tiantian.gift.model.Gift;
import sun.security.provider.MD5;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class LocalCache {

    static {


        Gift gift = new Gift();
        gift.setGiftCode("money");
        gift.setGiftMark("10000");
        gift.setGiftNum(10000);
        gift.setGiftImg("http://pw-public.oss-cn-shenzhen.aliyuncs.com/user_avatar/20160329/56fa2e18e63751144b7cfa35.png");
        gift.setGiftType("virtual");
        gift.setGiftClass("goods");

        Gift gift1 = new Gift();
        gift1.setGiftCode("money");
        gift1.setGiftMark("30000");
        gift1.setGiftNum(30000);
        gift1.setGiftImg("http://pw-public.oss-cn-shenzhen.aliyuncs.com/user_avatar/20160329/56fa2e18e63751144b7cfa35.png");
        gift1.setGiftType("virtual");
        gift1.setGiftClass("goods");

        Gift gift2 = new Gift();
        gift2.setGiftCode("money");
        gift2.setGiftMark("50000");
        gift2.setGiftNum(50000);
        gift2.setGiftImg("http://pw-public.oss-cn-shenzhen.aliyuncs.com/user_avatar/20160329/56fa2e18e63751144b7cfa35.png");
        gift2.setGiftType("virtual");
        gift2.setGiftClass("goods");

        Gift gift3 = new Gift();
        gift3.setGiftCode("pyc_rcq1h");
        gift3.setGiftImg("http://pw-public.oss-cn-shenzhen.aliyuncs.com/user_avatar/20160329/56f9fbc1e63751144b7cfa0a.png");
        gift3.setGiftNum(1);
        gift3.setGiftMark("1小时");
        gift3.setGiftType("virtual");
        gift3.setGiftClass("friend_tick");


        Gift gift4 = new Gift();
        gift4.setGiftCode("pyc_rcq2h");
        gift4.setGiftNum(1);
        gift4.setGiftMark("2小时");
        gift4.setGiftImg("http://pw-public.oss-cn-shenzhen.aliyuncs.com/user_avatar/20160329/56f9fbc1e63751144b7cfa0a.png");
        gift4.setGiftType("virtual");
        gift4.setGiftClass("friend_tick");


        Gift gift5 = new Gift();
        gift5.setGiftCode("pyc_rcq2h");
        gift5.setGiftNum(1);
        gift5.setGiftMark("2小时");
        gift5.setGiftImg("http://pw-public.oss-cn-shenzhen.aliyuncs.com/user_avatar/20160329/56f9fbc1e63751144b7cfa0a.png");
        gift5.setGiftType("virtual");
        gift5.setGiftClass("friend_tick");


        Gift gift6 = new Gift();
        gift6.setGiftCode("pyc_rcq2h");
        gift6.setGiftNum(1);
        gift6.setGiftMark("2小时");
        gift6.setGiftImg("http://pw-public.oss-cn-shenzhen.aliyuncs.com/user_avatar/20160329/56f9fbc1e63751144b7cfa0a.png");
        gift6.setGiftType("virtual");
        gift6.setGiftClass("friend_tick");


        Gift gift7 = new Gift();
        gift7.setGiftCode("money");
        gift7.setGiftNum(100000);
        gift7.setGiftMark("100000");
        gift7.setGiftImg("http://pw-public.oss-cn-shenzhen.aliyuncs.com/user_avatar/20160329/56fa2e18e63751144b7cfa35.png");
        gift7.setGiftType("virtual");
        gift7.setGiftClass("goods");
//
//        signMap2.put("1", gift);
//        signMap2.put("2", gift2);
//        signMap2.put("3", gift3);
//        signMap2.put("4", gift4);
//        signMap2.put("5", gift5);
//        signMap2.put("6", gift6);
//        signMap2.put("7", gift7);
//
//
//        signBonusMap2.put("1", gift);
//        signBonusMap2.put("2", gift2);
//        signBonusMap2.put("3", gift3);

//        signMap = signMap2;
//
//        signBonusMap = signBonusMap2;
    }

//    private static final Map<String, Gift> signBonusMap;
//
//    private static final Map<String, Gift> signMap;
    // 10分钟的有效时间
    private static final Cache<String, Optional<Props>> propsCache = CacheBuilder
            .newBuilder()
            .expireAfterAccess(600, TimeUnit.SECONDS)
            .build();

    // 5分钟的有效时间
    private static final Cache<String, Optional<String>> GIF_MD5 = CacheBuilder
            .newBuilder()
            .expireAfterAccess(300, TimeUnit.SECONDS)
            .build();

    // 10分钟的有效时间
    private static final Cache<String, Optional<Goods>> goodsCache = CacheBuilder
            .newBuilder()
            .expireAfterAccess(600, TimeUnit.SECONDS)
            .build();

    private static final Cache<String, Optional<Map<String, Gift>>> signBonusCache = CacheBuilder.newBuilder()
                        .expireAfterAccess(600, TimeUnit.SECONDS)
                        .build();

    private static final Cache<String, Optional<Map<String, Gift>>> signCache = CacheBuilder.newBuilder()
            .expireAfterAccess(600, TimeUnit.SECONDS)
            .build();

    public static Props getProps(final String code) {
        if (code == null) {
            return null;
        }
        try {
            Optional<Props> optional = propsCache.get(code, new Callable<Optional<Props>>() {
                @Override
                public Optional<Props> call() throws Exception {
                    List<Props> propsList = CacheUtils.getAllProps();
                    if (propsList != null && propsList.size() > 0) {
                        for (Props props : propsList) {
                            if (props.getCode().equalsIgnoreCase(code)) {
                                return Optional.fromNullable(props);
                            }
                        }
                    }
                    return Optional.absent();
                }
            });
            if (optional.isPresent()) {
                return optional.get();
            } else {
                propsCache.invalidate(code);
                return null;
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String giftMD5() {
        try {
            Optional<String> optional = GIF_MD5.get("gift_md5", new Callable<Optional<String>>() {
                @Override
                public Optional<String> call() throws Exception {
                    String str1 = JSON.toJSONString(getSignBonusGiftMap());
                    String str2 = JSON.toJSONString(getSignGiftMap());
                    String str = getMD5Str(str1 + str2);
                    return Optional.fromNullable(str);
                }
            });
            if (optional.isPresent()) {
                return optional.get();
            } else {
                GIF_MD5.invalidate("gift_md5");
                return "";
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            return "";
        }

    }

    /**
     * MD5 加密
     */
    private static String getMD5Str(String str) {
        MessageDigest messageDigest = null;

        try {
            messageDigest = MessageDigest.getInstance("MD5");

            messageDigest.reset();

            messageDigest.update(str.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException caught!");
            System.exit(-1);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        byte[] byteArray = messageDigest.digest();

        StringBuffer md5StrBuff = new StringBuffer();

        for (int i = 0; i < byteArray.length; i++) {
            if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)
                md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
            else
                md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
        }

        return md5StrBuff.toString();
    }

    public static Goods getGoods(final String code) {
        if (code == null) {
            return null;
        }
        try {
            Optional<Goods> optional = goodsCache.get(code, new Callable<Optional<Goods>>() {
                @Override
                public Optional<Goods> call() throws Exception {
                    List<Goods> goodsList = CacheUtils.getAllGoods();
                    if (goodsList != null && goodsList.size() > 0) {
                        for (Goods goods : goodsList) {
                            if (goods.getCode().equalsIgnoreCase(code)) {
                                return Optional.fromNullable(goods);
                            }
                        }
                    }
                    return Optional.absent();
                }
            });
            if (optional.isPresent()) {
                return optional.get();
            } else {
                goodsCache.invalidate(code);
                return null;
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Gift getSignBonusGift(String code) {

        return getSignBonusGiftMap().get(code);
    }

    public static Gift getSignGift(String code) {

        return getSignGiftMap().get(code);
    }

    public static Map<String, Gift> getSignGiftMap() {
        try {
            Optional<Map<String, Gift>> signMapOptional = signCache.get("sign_cache_key", new Callable<Optional<Map<String, Gift>>>() {
                @Override
                public Optional<Map<String, Gift>> call() throws Exception {
                    Map<String, Gift> giftMap = CacheUtils.getSignGifts();
                    if (giftMap != null) {
                        return Optional.fromNullable(giftMap);
                    }
                    return Optional.absent();
                }
            });
            if (signMapOptional.isPresent()) {
                return signMapOptional.get();
            } else {
                signCache.invalidate("sign_cache_key");
                return new HashMap<>();
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            return new HashMap<>();
        }
    }

    public static Map<String, Gift> getSignBonusGiftMap() {
        try {
            Optional<Map<String, Gift>> signBonusMapOptional = signBonusCache.get("sign_bonus_cache_key", new Callable<Optional<Map<String, Gift>>>() {
                @Override
                public Optional<Map<String, Gift>> call() throws Exception {
                    Map<String, Gift> giftMap = CacheUtils.getSignBonusGifts();
                    if (giftMap != null) {
                        return Optional.fromNullable(giftMap);
                    }
                    return Optional.absent();
                }
            });
            if (signBonusMapOptional.isPresent()) {
                return signBonusMapOptional.get();
            } else {
                signBonusCache.invalidate("sign_bonus_cache_key");
                return new HashMap<>();
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            return new HashMap<>();
        }
    }
}
