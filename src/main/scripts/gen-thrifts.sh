#!/bin/bash
SRC_DIR=$(cd `dirname $0` && cd .. && pwd -P)
THRIFTS_DIR=$SRC_DIR/thrifts
OUTPUT_DIR=$SRC_DIR/java
thrift --gen java -out $OUTPUT_DIR $THRIFTS_DIR/gift.thrift
thrift --gen go -out $OUTPUT_DIR $THRIFTS_DIR/gift.thrift