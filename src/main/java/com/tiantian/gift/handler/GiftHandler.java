package com.tiantian.gift.handler;

import com.tiantian.core.proxy_client.AccountIface;
import com.tiantian.gift.cache.LocalCache;
import com.tiantian.gift.cache.model.Goods;
import com.tiantian.gift.data.postgresql.PGDatabase;
import com.tiantian.gift.data.postgresql.design.UserSignTable;
import com.tiantian.gift.model.Gift;
import com.tiantian.gift.model.UserSign;
import com.tiantian.gift.thrift.*;
import com.tiantian.gift.utils.DateUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 *
 */
public class GiftHandler implements GiftService.Iface {
    static Logger LOG = LoggerFactory.getLogger(GiftHandler.class);
    @Override
    public String getServiceVersion() throws TException {
        return giftConstants.version;
    }

    public UserSignInfo userSignInfo(String userId) {
        UserSign userSign = getUserSign(userId);
        if (userSign == null) {
            userSign = saveUserSign(userId);
        }
        boolean needUpdate = false;
        // 如果不是同一周,重置信息
        if (!DateUtils.checkChinaSameWeek(userSign.getUpdateTime(), System.currentTimeMillis())) {
            userSign.setBonus("0,0,0");
            userSign.setSign("0,0,0,0,0,0,0");
            userSign.setUpdateTime(System.currentTimeMillis());
            needUpdate = true;
        }
        // 判断是否需要修改未签到的值
        String rightSign = getRightSign(userSign.getSign());
        if (!rightSign.equals(userSign.getSign())) {
            // 更新
            userSign.setSign(rightSign);
            needUpdate = true;
        }

        if (needUpdate) {
            updateUserSign(userSign.getId(), userSign.getSign());
        }
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String updateTime = sf.format(new Date(userSign.getUpdateTime()));
        return new UserSignInfo(userSign.getId(), userSign.getUserId(), userSign.canSign(), userSign.canBonus(),
                updateTime);
    }


    // 玩家签到
    public Map<String, GiftInfo> userSign(String userId) {
        Map<String,  GiftInfo> retMap = new HashMap<>();
        UserSignInfo userSign = userSignInfo(userId);
        retMap.put("sign", new GiftInfo());
        retMap.put("bonus", new GiftInfo());
        if (userSign == null) {
            return retMap;
        }
        String sign = userSign.getSign();
        String[] sings = sign.split(",");
        int signIndex = 0;
        for (int i = 0 ; i< sings.length; i ++) {
             if (sings[i].equalsIgnoreCase("2")) {
                 signIndex = i + 1;
                 break;
             }
        }
        if (signIndex > 0) {//领取签到奖励
            Gift gift = LocalCache.getSignGift(signIndex + "");
            if (gift != null) {
                Gift signGift = addGift(gift, userId);
                retMap.put("sign", transferGifts(signGift));
            }
        }

        String bonus = userSign.getBonus();
        String[] bonusStrs = bonus.split(",");
        int bonusIndex = 0;
        for (int i = 0 ; i< bonusStrs.length; i ++) {
            if (bonusStrs[i].equalsIgnoreCase("2")) {
                bonusIndex = i + 1;
                break;
            }
        }

        if (bonusIndex > 0) { // 领取额外奖励
            Gift gift = LocalCache.getSignBonusGift(bonusIndex + "");
            if (gift != null) {
                Gift signGifts = addGift(gift, userId);
                retMap.put("bonus", transferGifts(signGifts));
            }
        }
        // 修改状态
        updateUserSignAndBonus(userSign.getId(), sign.replace("2", "1"), bonus.replace("2", "1"));
        return retMap;
    }

    public CommonSignInfo getCommonGiftInfo() {
        return new CommonSignInfo(getSignGifts(), getBonusGifts());
    }

    public String giftMd5() {
       return LocalCache.giftMD5();
    }

    private GiftInfo transferGifts(Gift gift) {
        if (gift == null) {
            return null;
        }
        GiftInfo giftInfo = new GiftInfo();
        giftInfo.setCode(StringUtils.isBlank( gift.getGiftCode()) ? "" :  gift.getGiftCode());
        giftInfo.setName(StringUtils.isBlank(gift.getGiftName()) ? "" : gift.getGiftName());
        giftInfo.setNumber(gift.getGiftNum());
        giftInfo.setImg(StringUtils.isBlank(gift.getGiftImg()) ? "" : gift.getGiftImg());
        giftInfo.setMark(StringUtils.isBlank(gift.getGiftMark()) ? "" : gift.getGiftMark());
        return giftInfo;
    }

    private Gift addGift(Gift gift, String userId) {
         try {
             String code = gift.getGiftCode();
             if (code.equalsIgnoreCase("money")) {
                 AccountIface.instance().iface().addUserMoney(userId, gift.getGiftNum());
             } else if (code.equalsIgnoreCase("diamond")) {
                 AccountIface.instance().iface().addUserDiamond(userId, gift.getGiftNum());
             } else {
                 //朋友场入场券
                 if ("friend_ticket".equalsIgnoreCase(gift.getGiftClass())) {
                     Goods goods = LocalCache.getGoods(code);
                     if (goods != null) {
                         String img = goods.getImgUrl();
                         if (StringUtils.isBlank(img)) {
                             img = "friend_ticket";
                         }
                         AccountIface.instance().iface().saveOrUpdateUserFriendTickets(userId, gift.getGiftCode(),
                                 img, goods.getDesc());
                     }
                 }
                 else if ("goods".equalsIgnoreCase(gift.getGiftClass())){ // goods
                    Goods goods = LocalCache.getGoods(code);
                    if (goods != null) {
                        AccountIface.instance().iface().saveOrUpdateUserProps(userId, goods.getCode(), goods.getName(),
                                goods.getImgUrl(), goods.getDesc(), gift.getGiftNum());
                    }
                 }
             }
         }
         catch (Exception e) {
             e.printStackTrace();
         }
        return gift;
    }

    private UserSign getUserSign(String userId) {
        Statement st = null;
        UserSign userSign = null;
        try {
            st = PGDatabase.getInstance().getStatement();
            String sql = String.format("SELECT * FROM %s WHERE %s = '%s' limit 1;",
                    UserSignTable.TABLE_USER_SIGN, UserSignTable.COLUMN_USER_ID, userId);
            LOG.info("select getUserSign page sql = {}", sql);
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                userSign = userSignFromSqlRS(rs);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (null != st){
                try {
                    Connection conn = st.getConnection();
                    conn.close();
                    st.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return userSign;
    }

    private UserSign saveUserSign(String userId) {
        UserSign userSign = null;
        String id = UUID.randomUUID().toString().replace("-", "");
        long current = System.currentTimeMillis();
        //建表时明确，默认为不激活
        String sql = String.format("INSERT INTO %s(%s, %s, %s, %s, %s, %s)" +
                        " VALUES('%s', '%s', '%s', '%s', %d, %d);",
                UserSignTable.TABLE_USER_SIGN,
                UserSignTable.COLUMN_ID,  UserSignTable.COLUMN_USER_ID, UserSignTable.COLUMN_SIGN,
                UserSignTable.COLUMN_BONUS, UserSignTable.COLUMN_CREATE_TIME, UserSignTable.COLUMN_UPDATE_TIME,
                id, userId, "0,0,0,0,0,0,0", "0,0,0", current, current);

        LOG.info("saveUserSign ======> sql is {}",sql);
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = PGDatabase.getInstance().getPreparedStatement(sql);
            int num = preparedStatement.executeUpdate();
            if (num == 1) {
                userSign = new UserSign();
                userSign.setId(id);
                userSign.setUserId(userId);
                userSign.setSign("0,0,0,0,0,0,0");
                userSign.setBonus("0,0,0");
                userSign.setCreateTime(current);
                userSign.setUpdateTime(current);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (null != preparedStatement){
                try {
                    Connection conn = preparedStatement.getConnection();
                    conn.close();
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return userSign;
    }

    private boolean updateUserSign(String id, String sign) {
        long current = System.currentTimeMillis();
        //建表时明确，默认为不激活
        String sql = String.format("UPDATE %s set %s = '%s', %s = %d " +
                        " where %s = '%s';",
                UserSignTable.TABLE_USER_SIGN,
                UserSignTable.COLUMN_SIGN, sign,
                UserSignTable.COLUMN_UPDATE_TIME, current,
                UserSignTable.COLUMN_ID, id);

        LOG.info("updateUserSign ======> sql is {}",sql);
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = PGDatabase.getInstance().getPreparedStatement(sql);
            int num = preparedStatement.executeUpdate();
            return num == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (null != preparedStatement){
                try {
                    Connection conn = preparedStatement.getConnection();
                    conn.close();
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    private boolean updateUserSignAndBonus(String id, String sign, String bonus) {
        long current = System.currentTimeMillis();
        //建表时明确，默认为不激活
        String sql = String.format("UPDATE %s set %s = '%s', %s = '%s', %s = %d " +
                        " where %s = '%s';",
                UserSignTable.TABLE_USER_SIGN,
                UserSignTable.COLUMN_SIGN, sign,
                UserSignTable.COLUMN_BONUS, bonus,
                UserSignTable.COLUMN_UPDATE_TIME, current,
                UserSignTable.COLUMN_ID, id);

        LOG.info("updateUserSignAndBonus ======> sql is {}",sql);
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = PGDatabase.getInstance().getPreparedStatement(sql);
            int num = preparedStatement.executeUpdate();
            return num == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (null != preparedStatement){
                try {
                    Connection conn = preparedStatement.getConnection();
                    conn.close();
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    private UserSign userSignFromSqlRS(ResultSet rs) throws SQLException {
        UserSign userSign = null;
        if (rs != null) {
            userSign = new UserSign();
            userSign.setId(rs.getString(UserSignTable.COLUMN_ID));
            userSign.setSign(rs.getString(UserSignTable.COLUMN_SIGN));
            userSign.setUserId(rs.getString(UserSignTable.COLUMN_USER_ID));
            userSign.setBonus(rs.getString(UserSignTable.COLUMN_BONUS));
            userSign.setCreateTime(rs.getLong(UserSignTable.COLUMN_CREATE_TIME));
            userSign.setUpdateTime(rs.getLong(UserSignTable.COLUMN_UPDATE_TIME));
        }
        return userSign;
    }

    private String getRightSign(String sign) {
        String[] signs = sign.split(",");
        Calendar calendar =  Calendar.getInstance();
        calendar.setTime(new Date());
        int index = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (index <= 0) {
            index = 7;
        }
        for (int i = 0; i < index - 1; i++) {
             String val = signs[i];
             if ("0".equalsIgnoreCase(val)) {
                 signs[i] = "-1";
             }
        }
        return StringUtils.join(signs, ",");
    }

    private List<GiftInfo> getSignGifts() {
        List<GiftInfo> result = new ArrayList<>();
        for (int i = 1; i <= 7 ; i++) {
             Gift gift = LocalCache.getSignGift(i + "");
             if (gift != null) {
                 result.add(transferGifts(gift));
             }
        }
        return result;
    }

    private List<GiftInfo> getBonusGifts() {
        List<GiftInfo> result = new ArrayList<>();
        for (int i = 1; i <= 7; i++) {
             Gift gift = LocalCache.getSignBonusGift(i + "");
             if (gift != null) {
                 result.add(transferGifts(gift));
             }
        }
        return result;
    }
}
