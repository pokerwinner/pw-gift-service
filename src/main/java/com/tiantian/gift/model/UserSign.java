package com.tiantian.gift.model;

import org.apache.commons.lang.StringUtils;

import java.util.Calendar;
import java.util.Date;

/**
 *
 */
public class UserSign {
    private String id;
    private String userId;
    private String sign; // 签到内容0,0,0,0,0,0,0 已签到1, 未签到是-1, 可以签到是(显示给客户端，数据库中没有该值)2
    private String bonus; // 0,0,0  已领取1, 可领取(显示给客户端，数据库中没有该值) 2; 周3, 5, 7 奖励
    private long createTime;
    private long updateTime;

    /**
     * 获取可以签到的信息
     * @return
     */
    public String canSign() {
       //判断今天是否签到过
        Calendar calendar =  Calendar.getInstance();
        calendar.setTime(new Date());
        int index = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (index <= 0) {
            index = 7;
        }
        String[] signStrs = sign.split(",");
        if (signStrs[index - 1].equalsIgnoreCase("0")) {
            return sign.replaceFirst("0", "2");
        } else {
            return sign;
        }
    }
    /**
     * 获取可以领取奖励的信息
     * @return
     */
    public String canBonus() {
       int conIndex = 0; // 连续几天签到 2 , 4 , 6
       int index = -1;
       String[] bonusSts = bonus.split(",");
       for (int i = 0; i < bonusSts.length; i++) {
            if ("0".equals(bonusSts[i])) {
                conIndex = 2 * i + 2;
                index = i;
                break;
            }
       }
       if(conIndex > 0) {
          String[] signStrs = sign.split(",");
          int cnt = 0;
          for (int a = 0; a < signStrs.length; a++) {
               String sign = signStrs[a];
               if ("1".equals(sign)) {
                   cnt++;
                   if (cnt >= conIndex) {
                       bonusSts[index] = "2";
                       break;
                   }
               } else {
                   cnt = 0;
               }
          }
       }
       return StringUtils.join(bonusSts, ",");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }
}
