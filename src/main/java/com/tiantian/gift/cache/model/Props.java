package com.tiantian.gift.cache.model;

/**
 *
 */
public class Props {
    private String code; // 编号
    private String name; // 名称
    private String imgUrl; // 图片URL
    private String desc; // 描述
    private String goodsCode; // 包含物品code
    private long numbers; // 物品数量
    // 消耗钻石数
    private long costDiamond;
    // 消耗金币数
    private long costMoney;
    private int order;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public long getNumbers() {
        return numbers;
    }

    public void setNumbers(long numbers) {
        this.numbers = numbers;
    }

    public long getCostDiamond() {
        return costDiamond;
    }

    public void setCostDiamond(long costDiamond) {
        this.costDiamond = costDiamond;
    }

    public long getCostMoney() {
        return costMoney;
    }

    public void setCostMoney(long costMoney) {
        this.costMoney = costMoney;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
