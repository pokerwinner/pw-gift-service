package com.tiantian.gift.model;

/**
 *
 */
public class Gift {
    private String giftCode;
    private String giftName;
    private String giftType; //virtual, physical
    private String giftClass; //分类: friend_tick, goods
    private long giftNum;
    private String giftMark;
    private String giftImg;

    public String getGiftCode() {
        return giftCode;
    }

    public void setGiftCode(String giftCode) {
        this.giftCode = giftCode;
    }

    public String getGiftImg() {
        return giftImg;
    }

    public void setGiftImg(String giftImg) {
        this.giftImg = giftImg;
    }

    public String getGiftMark() {
        return giftMark;
    }

    public void setGiftMark(String giftMark) {
        this.giftMark = giftMark;
    }

    public String getGiftType() {
        return giftType;
    }

    public void setGiftType(String giftType) {
        this.giftType = giftType;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public long getGiftNum() {
        return giftNum;
    }

    public void setGiftNum(long giftNum) {
        this.giftNum = giftNum;
    }

    public String getGiftClass() {
        return giftClass;
    }

    public void setGiftClass(String giftClass) {
        this.giftClass = giftClass;
    }
}
