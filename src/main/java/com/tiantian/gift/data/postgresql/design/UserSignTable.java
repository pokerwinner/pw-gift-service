package com.tiantian.gift.data.postgresql.design;

/**
 * Created by jeffma on 15/10/26.
 */
public class UserSignTable {
    public static final String TABLE_USER_SIGN = "public.user_sign";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_SIGN = "sign";
    public static final String COLUMN_BONUS = "bonus";
    public static final String COLUMN_CREATE_TIME = "create_time";
    public static final String COLUMN_UPDATE_TIME = "update_time";
}
