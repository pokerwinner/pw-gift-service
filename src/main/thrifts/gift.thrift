namespace java com.tiantian.gift.thrift

const string version = '1.0.1'

struct GiftInfo {
    1:string code
    2:string name
    3:i64 number
    4:string img
    5:string mark
}

struct UserSignInfo {
      1:string id
      2:string userId
      3:string sign
      4:string bonus
      5:string updateTime
}

struct CommonSignInfo {
      1:list<GiftInfo> signGifts
      2:list<GiftInfo> bonusGifts
}


service GiftService {
    string getServiceVersion(),
    UserSignInfo userSignInfo(1:string userId)
    map<string, GiftInfo> userSign(1:string userId),
    CommonSignInfo getCommonGiftInfo(),
    string giftMd5()
}