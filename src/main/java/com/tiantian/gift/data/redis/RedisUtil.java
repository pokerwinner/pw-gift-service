package com.tiantian.gift.data.redis;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public class RedisUtil {
    public static String get(String key) {
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            result = jedis.get(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return result;
    }

    public static <T> T getObject(String key, Class<T> tClass) {
        Jedis jedis = null;
        T result = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            String jsonResult = jedis.get(key);
            if (StringUtils.isNotBlank(jsonResult)) {
                result = JSON.parseObject(jsonResult, tClass);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return result;
    }

    public static <T> List<T> getObjectArrays(String key, Class<T> tClass) {
        Jedis jedis = null;
        List<T> result = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            String jsonResult = jedis.get(key);
            if (StringUtils.isNotBlank(jsonResult)) {
                result = JSON.parseArray(jsonResult, tClass);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return result;
    }

    public static void save(String key, String val) {
        Jedis jedis = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            jedis.set(key, val);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    public static void saveObject(String key, Object val) {
        Jedis jedis = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            jedis.set(key, JSON.toJSONString(val));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    public static void setMap(String key, String field, String val) {
        Jedis jedis = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            jedis.hset(key, field, val);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    public static String getFromMap(String key, String field) {
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            result = jedis.hget(key, field);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return result;
    }

    public static Map<String, String> getMap(String key) {
        Jedis jedis = null;
        Map<String, String> result = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            result = jedis.hgetAll(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return result;
    }

    public static Set<String> getKeys(String keyPattern) {
        Jedis jedis = null;
        Set<String> result = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            result = jedis.keys(keyPattern);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return result;
    }


    public static Set<String> getMapKeys(String key) {
        Jedis jedis = null;
        Set<String> result = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            result = jedis.hkeys(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return result;
    }

    public static boolean exists(String key) {
        Jedis jedis = null;
        boolean result = false;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            result = jedis.exists(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return result;
    }

    public static void del(String key) {
        Jedis jedis = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            jedis.del(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }
    // 获取set集合中的值
    public static Set<String> smembers(String key) {
        Jedis jedis = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            return jedis.smembers(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();

            }
        }
        return null;
    }

    // 获取set集合中的元素个数
    public static long scard(String key) {
        Jedis jedis = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            return jedis.scard(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();

            }
        }
        return 0;
    }

    //判断值是否是set的member
    public static boolean sismember(String key, String val) {
        Jedis jedis = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            return jedis.sismember(key, val);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();

            }
        }
        return false;
    }
    //移除一个值
    public static long srem(String key, String val) {
        Jedis jedis = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            return jedis.srem(key, val);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();

            }
        }
        return 0;
    }
    // 增加一个值
    public static long sadd(String key, String val) {
        Jedis jedis = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            return jedis.sadd(key, val);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();

            }
        }
        return 0;
    }

    // 自增1
    public static long incr(String key) {
        Jedis jedis = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            return jedis.incr(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();

            }
        }
        return -1;
    }

    // 自减1
    public static long decr(String key) {
        Jedis jedis = null;
        try {
            jedis = RedisConnection.getInstance().getConnection();
            return jedis.decr(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();

            }
        }
        return -1;
    }
}

